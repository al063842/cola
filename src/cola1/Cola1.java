/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cola1;

import java.util.LinkedList;
import java.util.Queue;

/**
 *
 * @author Rodrigo Balan
 */
public class Cola1 {

    public static void main(String[] args) {
        System.out.println("Gaytan Balan Rodrigo Manuel - 63842");
        cola1();
        cola2();
        cola3();

    }

    public static void cola1() {

        System.out.println("--------");
        System.out.println("Ejercicio 1");
        String paren = "((pila.size)== 0) && flag))";
        Queue<Character> cola1 = new LinkedList();
        boolean flag = true;
        char[] string1 = paren.toCharArray();
        for (char parentesis : string1) {
            if (parentesis == '(') {
                cola1.add(parentesis);
                System.out.println("Se inserto: " + parentesis);
            }
            if (parentesis == ')') {
                if (cola1.poll() != null) {
                    cola1.peek();
                    System.out.println("Se extrajo: " + parentesis);
                } else {
                    flag = false;
                }
            }

        }

        if (cola1.size() == 0 && flag) {
            System.out.println("Los parentesis estan colocados de manera correcta");
        } else {
            System.out.println("Los parentesis estan colocados de manera incorrecta");
        }

    }

    public static void cola2() {

        System.out.println("--------");
        System.out.println("Ejercicio 2");
        String mark = "<b><i>Hola FI</i>/b>";
        Queue<Character> cola2 = new LinkedList();
        boolean flag = true;
        char[] string1 = mark.toCharArray();
        for (char marktext : string1) {
            if (marktext == '<') {
                cola2.add(marktext);
                System.out.println("Se inserto: " + marktext);
            }
            if (marktext == '>') {
                if (cola2.poll() != null) {
                    cola2.peek();
                    System.out.println("Se extrajo: " + marktext);
                } else {
                    flag = false;
                }
            }

        }

        if (cola2.size() == 0 && flag) {
            System.out.println("Los indicadores de marktext estan colocados de manera correcta");
        } else {
            System.out.println("Los indicadores de marktext estan colocados de manera incorrecta");
        }
    }

    public static void cola3() {
        System.out.println("--------");
        System.out.println("Ejercicio 3");

    }

}
